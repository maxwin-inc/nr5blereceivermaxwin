Pod::Spec.new do |s|
    s.name     = 'NR5BLEReceiver'
    s.version  = '1.0.23'
    s.license  = 'MIT'
    s.summary  = "..."
    s.homepage = 'https://bitbucket.org/maxwin-inc/NR5BLEReceiverDATA1968'
    s.authors  = { 'yume190' => 'yume190@gmail.com' }
    s.social_media_url = "https://www.facebook.com/yume190"
    s.source   = { :git => 'https://bitbucket.org/maxwin-inc/NR5BLEReceiverDATA1968', :tag => s.version }

    s.ios.deployment_target = '9.0'
    s.ios.vendored_frameworks = 'NR5BLEReceiver.framework', 'BLEData.framework'
    # s.source_files = ['NR5BLEReceiver.framework']
    # s.source_files = "BLEData/YumeData.h"
    # s.module_map = "BLEData/module.modulemap"    

    s.swift_version = '5.0'


end
